<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="icon" type="image/png" sizes="16x16" href="/jenny/twitter/assets/img/favicon-16x16.png">
  <title>Chill Koala Tweeting</title>

  <link rel="stylesheet" href="/jenny/twitter/assets/style.css">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300" rel="stylesheet">
</head>
<body>

  <section class="main">
    <div class="box load">
      <div class="profile">
        <img src="/jenny/twitter/assets/img/profile.jpg" alt="">
      </div>

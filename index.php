<?php 
include "includes/header.php"; 
$error = isset($_GET['error']);
?>

      <h1>Chill Koala wants to tweet.</h1>
      <p>Write something for him.</p>
      <span>"Why?" You might ask. Just <a href="https://youtu.be/FQRW0RM4V0k" target="_blank">do it</a>.</span>

      <form action="post.php" method="post">
        <textarea name="tweet" rows="2" cols="80" placeholder="I don't know... maybe a weird koala fact?" required></textarea>
        <?php if ($error == 1) {
          echo "<span class='error'>You gotta write something!</span>";
        } ?>
        <button type="submit" class="button" name="button">Tweet</button>
      </form>


<?php include "includes/footer.php"; ?>
